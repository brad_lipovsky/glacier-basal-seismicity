function  [x_keep, L_keep, count, alpha_keep] = mcmc(func,data,x0,xstep,xbnds,...
                 sigma,Niter,L0)
%
% [x_keep, L_keep, count] = mcmc(func,data,x0,xstep,sigma,Niter,varargin)
%
% subroutine for MCMC sampling using Metropolis-Hasting w/ normal
% distribution.
%
% Inputs:
%       func = function name eg:   mcmc('travel_time',....)
%               parameter required for function in varargin
%       data = vector of observations
%       x0   = initial estimate of parameter vector
%       xstep = step size in all parameter directions
%       xbnds = bounds (
%       sigma = sigma of normal distribution 
%       Niter = number of iterations
%
% Outputs:
%       x_keep = array of samples
%       L_keep = likelihood of samples
%       count  = number of accepted. Acceptance ratio is count/Niter
%
%  P Segall: 2012
% TODO? Should I add something to seed rand? Better if this is done in
% calling program
% rng('shuffle');

fun  = fcnchk(func);

%number of elements in x
Nparams = length(x0);
% check dimensions of bounds
if( size(xbnds,1) ~= Nparams |  size(xbnds,2) ~= 2)
    disp('Dimension of xbnds is not valid')
    return
end
% TODO could check that x0 lies within bounds

x_keep=zeros(Nparams,Niter); 
L_keep=zeros(1,Niter); 
alpha_keep = zeros(1,Niter); 

x = x0;
L = L0;

count=0;
for k=1:Niter
disp(' ');
disp(['Starting iteration ' num2str(k) ' of ' num2str(Niter)]);
    
    % generate proposal
    xprop = x + xstep.* 2 .* (rand(Nparams,1)-0.5);
    
    % check bounds
    if (max(xprop > xbnds(:,1)) || max(xprop < xbnds(:,2)))

        % prediction
        dprop = fun(xprop);

        % likelihood
        Lprop = -0.5/sigma^2 * norm( (data-dprop)./data ) ^2;

        u=rand;
        if (L==0 || u <= exp(Lprop-L))
             count=count+1;
             x = xprop;
             L = Lprop;
        end 
    
    end
    x_keep(:,k) = x;
    L_keep(k) = L;
    alpha_keep(k) = exp(Lprop-L);
        
%     plot(L_keep,'ok');
%     drawnow;
end


