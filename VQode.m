function dY = VQode(t,Y,M)
  
% VQODE returns V, dQ, dV as a column vector

D = Y(1);
Q = Y(2);

[V,dQ,~] = rates(D,Q,t,M);
dY = [V; dQ];
  
