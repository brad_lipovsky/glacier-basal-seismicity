function M = LoadParams(situ)
%
% This file sets the parameters for various cases.  The default parameters
% are listed first.  These are then modified for each particular case
% below.
%

% Default parameters
% situ = 'CompliantBed';
M.geom = 'wholespace';

M.f0 = 0.4;             % static coefficient of friction
M.FarfieldVelocity = 1; % Interpret output velocities as far field 
                        %   velocities (set to zero to interpret as sliding velocities)
M.Q = 500;              % quality factor
M.c_ice = 2000;         % wavespeed
M.G_ice = 3664;         % shear modulus (MPa)
M.H = 800;              % source-reciever distance
M.V0 = 10^-5;
M.dt = 0.5e-3;          % default time step
M.nui = 0.25;           % Poisson ratio in upper material
M.nub = 0.25;           % Poisson ratio in lower material
M.WindowDuration = 10;
M.a = 0.005;
M.b = 0.015;
M.G_till = 100;
M.rho_till = 1700;
M.L = 1e-6;

M.fs = 100; % Seismometer sampling rate
M.dRdt = 0;
M.load = 'constant';
M.SimulationDuration = 60;

% Integration tolerances
M.rtol = 1e-7; % relative tolerance
M.atol = 1e-7; % absolute tolerance

switch situ
    % Particular cases
    
        case 'SlowSlip'
        M.H = 4e3;
        
        M.FarfieldVelocity = 0; % Interpret velocities as sliding velocities (much faster...)
        M.Vs = 10 /86400/365;
        M.V0 = M.Vs;
        M.L = 1;
        M.R = 200e3;
        M.N = 0.0256 * 35;              % Effective normal stress (MPa)
        M.a = 0.01;
        M.b = 0.04;
        
        M.c_ice = 2000;         % wavespeed
        M.G_ice = 3664;         % Shear modulus (MPa)
        M.G_till = M.G_ice;     % Shear modulus (MPa) is identical on both sides
        M.nui = 0.33;   % Poisson ratio in upper material
        M.nub = M.nui;   % Poisson ratio in lower material

    
        case 'SanAndreas'
   		M.FarfieldVelocity = 0; % Interpret velocities as sliding velocities (much faster...)
        M.Vs = 0.4/3.14e7;      % Far-field loading rate 40cm/yr
        M.c_ice = 4000;         % wavespeed
        M.G_ice = 50e4;         % Shear modulus (MPa)
        M.G_till = M.G_ice;     % Shear modulus (MPa) is identical on both sides
        M.R = 10e3;             % Fault Size (m)
        M.N = 200;              % Effective normal stress (MPa)
        M.L = 1e-3;             % Frictional state evolution distance (aka D sub C)
        
        case 'DavidGlacier'
            
        M.L = 1e-3;
        M.R = 33;
        M.N = 1.5;
        M.G_till = 3664; 
        M.G_till = 3664 * 2 / ( 1 + coth(M.R/1600) );  % Includes effect
                                            % of variable ice thickness.
        
        M.H = 180e3; % Source-to-receiver distance
        M.Q = 100;  % quality factor
        M.Vs = 1.6e-5;
        M.G_ice  = M.G_till;
        M.nui = 0.25;   M.nub = M.nui;
        M.rho_till = 2000; M.c_ice = 2000;
      
        case 'CompliantBed'
        M.nui = 0.33;           % Poisson ratio in upper material
        M.nub = 0.49;           % Poisson ratio in lower material
        M.Vs = 4.86e-4;
        M.G_till = 10;
        M.R = 3.3;
        M.N = 0.04;
        
        case 'RigidBed'
        M.Vs = 4.86e-4;
        M.R = 3.3;
        M.N = 0.1;
%         M.D = M.Vs/M.f1;
        
        case 'VeryUnstable'
            M.Vs = 4.86e-4;
            M.R = 100;
            M.N = 0.1;
            M.SimulationDuration = 1e2;
        
        case 'SubductionZone'
        
            % Both sides of the fault have the same material
            M.G_ice = 1e4; M.G_till = M.G_ice;
            M.nui = 0.25; M.nub = 0.25;

            % 10m fault radius
            M.R = 10;

            % Normal stress is 1 kPa
            M.N = 1;

            % source-reciever distance
            M.H = 15e3;
        
        case 'landslide_accel'
            
        	% These quantities are fixed from observations:
            M.H = 3.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'acceleration';
            M.As = 1e-5 / 60; M.Vs = 0.6e-3; M.V0=M.Vs;
            M.R = 40;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 15e-6;
            M.a = 0.03;
            M.b = 0.04;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
              
%             M.SimulationDuration = 60;
            % Use this to match data:
            M.SimulationDuration = 45*60;
            
        case 'landslide_decel'
            
        	% These quantities are fixed from observations:
            M.H = 3.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'acceleration';
            M.As = -1e-5 / 60; M.Vs = 0.6e-3; M.V0=M.Vs;
            M.R = 40;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 15e-6;
            M.a = 0.03;
            M.b = 0.04;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
              
            M.SimulationDuration = 60;
            % Use this to match data:
%             M.SimulationDuration = 45*60;
    
            
        case 'landslide2'
            
        	% These quantities are fixed from observations:
            M.H = 3.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'acceleration';
            M.As = 1e-5 / 60; M.Vs = 0.6e-3;
            M.R = 45;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 15e-6;
            M.a = 0.03;
            M.b = 0.04;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
                        
            M.SimulationDuration = 45*60;
            
    case 'landslide_growingpatch'
            
        	% These quantities are fixed from observations:
            M.H = 3.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'constant';
%             M.As = 2e-5 / 60; 
            M.Vs = 0.6e-3;
%             M.tAcc = 6*60;
            M.R = 40;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 10e-6;
            M.a = 0.03;
            M.b = 0.037;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
                        
            M.SimulationDuration = 15*60;
            
            M.dRdt = 5e-2;
            
        case 'landslide_layereffect'
            
        	% These quantities are fixed from observations:
            M.H = 3.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

            % Quantities that we may vary to match the data:
            M.load = 'constant';
%             M.As = 1e-4 / 60; 
            M.Vs = 2e-3;
            M.R = 200;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 15e-6;
            M.a = 0.03;
            M.b = 0.04;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
                        
            M.dRdt = 1e-1;
            
            M.SimulationDuration = 10*60;
            M.geom = 'layer';
            M.LayerH = 30;      % Layer thickness
            
case 'landslide_short'
            
        	% These quantities are fixed from observations:
            M.H = 0.5e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       
            M.fs = 500;

            
            % Quantities that we may vary to match the data:
            M.load = 'velocityFit';
            M.Vs = 1e-3;
            M.R = 10;
            M.N = 0.5; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 5 m         
            M.L = 1e-6;
            M.a = 0.03;
            M.b = 0.05;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 100; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
                        
            M.SimulationDuration = 3;
            
%             M.dRdt = 0.01;
            
            tt = 0:0.01:3;
            vv = M.Vs*exp(-(tt-1.5).^2/0.5);
            M.VeloFit = spline(tt,vv);
            
            case 'rockglacier'
            
        	% These quantities are fixed from observations:
            M.H = 1e2;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'constant';
            M.Vs = 1e-7; M.V0=M.Vs;
            M.R = 4;
            M.N = 0.588; % Normal Stress  = overburden of 
                                     % unconsolidated rock with height 30 m         
            M.L = 20e-6;
            M.a = 0.03;
            M.b = 0.04;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 25; % Menke et al., Olafsson et al. suggest 300, a 
                        % relatively high value for crustal Q.
              
            M.SimulationDuration = 1e5;
            % Use this to match data:
%             M.SimulationDuration = 45*60;

            case 'argentiere'
            
        	% These quantities are fixed from observations:
            M.H = 1e3;        % Epicentral distance
            M.c_ice = 1878;     % Bed shear wavespeed
            M.rho_till = 2000;  % Bed density
            M.G_till = 7000;    % shear modulus
            M.G_ice = 7000;     % shear modulus (MPa)
            % These assume that the landslide and the bed have the same
            % material parameters.  Probably an okay starting point.

                       

            
            % Quantities that we may vary to match the data:
            M.load = 'constant';
            M.Vs = 14/365.25/86400; M.V0=M.Vs;
            M.R = 2.5;
            M.N = 0.4; % Effective stress
            M.L = 1e-6;
            M.a = 0.01;
            M.b = 0.02;
            M.f0 = 0.7;             % static coefficient of friction

            M.Q = 400; 
              
            M.SimulationDuration = 1.5e3;
            % Use this to match data:
%             M.SimulationDuration = 45*60;
end




% Derived quantities
M = DerivedParams(M);

end

