function [tr,dt,De,t,Q,tq] = RunSlider(M)

%
% 1. Calculate the interface stiffness. 
%
%    -  If patch is changing size (dR/dt ~=0) then k will be re-evaluated 
%       in step 2.
%    -  If layer effects are turned on, then an additional multiplicative
%       factor accounts for the increased geometrical compliance.
%
if strcmpi(M.geom,'layer'), 
    GeometryFactor = 2/( 1 + coth(2*pi*M.LayerH/M.R) );
else
    GeometryFactor = 1; 
end

M.k = GeometryFactor./M.R ...
        .* bimat(M.G_ice,M.G_till,M.nui,...
                M.nub,1e6*M.G_ice/M.c_ice^2,M.rho_till); 
% M.k = 7*pi*M.G./(16*M.R);    % "Eshelby" identical materials limit




%
% 2.  Calculate the evolution of friction and sliding velocity on the fault
%     interface.
M.Q0    = -M.b*log(M.Vs/M.V0);          % initial state
T0      = (M.f0+(M.a-M.b)*log(M.Vs/M.V0))*M.N; % initial shear stress (MPa)
M.D0    = -T0/M.k;                      % initial slip (m), set from initial 
                                        % stress
Y0      = [M.D0; M.Q0];                 % intial conditions
opt = odeset('InitialStep',M.dt,'Vectorized','on',...
    'RelTol',M.rtol,'AbsTol',M.atol,'OutputFcn',@outfunc);
sol = ode45(@VQode,[0 M.SimulationDuration],Y0,opt,M);
tq = sol.x;  [Y,dY] = deval(tq,sol);
D = Y(1,:); Q = Y(2,:); V = dY(1,:); dQdt = dY(2,:); 




%
% 3.  Wave propagation to the seismometer
%
% The velocity returned in "tr" can either be interpreted 
% as a far-field velocity (e.g., as recorded by a seismometer), 
% or it can be interpreted as a fault sliding velocity.  
%
% If interpreted as a far-field velocity (the default), 
% then wave propagation is calculated with attenuation.  
% If a sliding velocity, then we just return the output of 
% the ode solver.

if M.FarfieldVelocity
    disp(' ');disp('Calculating wave propagation.');
	% High-frequency amplitude calculation
	fs2 = 1/min(diff(tq));  dt = 1/fs2; 
	t2 = min(tq):dt:max(tq);
	N2 = numel(t2); if mod(N2,2), t2 = min(t2):dt:max(t2)-dt; end
    A = gradient(V,tq);% slip acceleration A=dV/dt
	A_interp = interp1(tq,A,t2);
    Z = M.z_till/(M.z_ice + M.z_till);

    % If the patch is changing size, then recalculate the patch size.
    if M.dRdt ~= 0
        R_var = M.R + M.dRdt*t2;
    else
        R_var = M.R;
    end
    
	v2 = Z * R_var.^2/(M.c_ice*M.H) .* ...
        farfield(A_interp,dt,M.R,M.H,M.c_ice,M.Q);

	% Don't decimate...
% 	tr = v2;
%     t = t2;
	
	% ... OR decimate to the seismometer frequency
    disp(' ');disp('Decimating synthetic seismogram to seismometer frequency.');
	tr = resample(v2,M.fs,round(fs2));
	dt = 1/M.fs; 
    t = dt*(0:(numel(tr)-1));

else
	tr = V;
    dt = -1; % return the full time vector instead.
end


% Return slip (D) per event as De
[~,val] = findpeaks(V);
De = mean(diff(D(val)));

% Is the energy dissipated during stick slip cycles equal to that
% dissipated during steady sliding?
% trapz(tq,(M.f0+Q).*V)/max(tq) / (M.f0*M.Vs)