%
% Rate- and state-friction spring slider system.  
%
% Please cite Lipovsky and Dunham (2016) if using this code 
% (doi:10.5194/tcd-9-1-2015).   The code is unique in that implements 
% quasi-dynamic elasticity for a bi-material  interface as described in the 
% previously mentioned reference. This code is based on an earlier code by 
% Dmitrieva et al. (2015, doi:10.1038/NGEO1879).
%

clear;

% Load the parameters
% M = LoadParams('landslide_decel');
% M = LoadParams('landslide_accel');
% M = LoadParams('rockglacier');
M = LoadParams('argentiere');

% Run the slider
tic; [vtr,~,D,t,Q,tq] = RunSlider(M); tt=toc;
disp(['Simulation done in ' num2str(tt) ' s.']);

% Make plots
figure; plot(t,vtr*1e9,'linewidth',2); hold on;
xlabel('Time (s)'); ylabel('Seismic Particle Velocity (nm/s)');
set(gca,'fontsize',18); axis tight; hold on;
yl=ylim; ylim( [min(yl(1)/2,1.25*yl(1)) 1.25*yl(2)]);

% figure;
% n = round(numel(vtr)/2^7);
% [S,F,T,P] = spectrogram(vtr,n,[],[],1/mean(diff(t)));
% surf(T/60,F,log10(P),'edgecolor','none'); view(2); %hcb=colorbar;
% xlabel('Time (min)');
% axis tight; ylim([0 25]); caxis([-16 -6]); colormap(jet); 
% ylabel('Frequency (Hz)');
% title('Spectral Power,  (m/s)^2/s')


dt = mean(diff(t));
[~,ind] = max(vtr);
plot (t(ind),vtr(ind)*1e9,'ok');

range = ind-(1/dt) : ind+(1/dt);
[ft,f] = bft(vtr(range),dt);

figure(2);
loglog(f,abs(ft./f));hold on;
xlabel('Frequency, Hz');
ylabel('Spectral Amplitude, m \times Hz');

[ft_all,f_all] = bft(vtr,dt);